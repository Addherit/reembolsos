<!DOCTYPE html>
<html lang="en">
    <?php include "../../includes/header.html" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg1 toggled"> 
            <?php include "../../assets/modales/modal-delete-confirmation.html" ?>
            <?php include "../../sidebar/index.html"?> 
            
            <!-- page-content  -->
            <main class="page-content pt-2">
              
            <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="col-12">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#" title="Ocultar/Mostrar menú">
                            <i class="fas fa-bars fa-lg"></i>
                          </a>
                            <span class="h1 align-middle" id="title-page">NUEVO USUARIO</span>
                        </div>                        
                    </div>
                    <hr> 
                    <br><br>
                   
                    <div class="row">
                        <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                            <form id="form-new-user"> 
                            
                                <div class="row">
                                    <label for="id-user" class="col-sm-2 col-form-label">ID</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control form-control-sm" id="id-user" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="user" class="col-sm-2 col-form-label">Usuario</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control form-control-sm" id="user">
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="name" class="col-sm-2 col-form-label">Nombre</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control form-control-sm" id="name">
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="rfc" class="col-sm-2 col-form-label">RFC</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control form-control-sm" id="rfc">
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="status" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control form-control-sm" id="status" value="Activo" readonly>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <label>Puedes agregar un excel para cargar múltiples usuarios</label>
                                <div class="row mb-2">
                                    <div class="col-12">
                                        <input type="file" name="btn-upload-xml" id="btn-upload-xml" class="inputfile" multiple>
                                        <label for="btn-upload-xml"> <i class="fas fa-upload"></i> Agregar excel</label>
                                    </div>
                                </div>
                                <hr>
                                <div class="row p-3">
                                    <div class="col-6 p-0"><button type="button" class="btn btn-secondary btn-block" data-dismiss="modal" aria-label="Close">Cancelar</button></div>
                                    <div class="col-6 p-0"><button type="submit" class="btn btn-success btn-block" id="btn-add-user">Crear usuario</button></div>
                                </div>                              
                            </form>
                        </div>
                    </div>
                  
                </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>

    <?php include "../../includes/footer.html" ?>    
    <script src="js/main.js" type="module"></script>

      
     
</html>