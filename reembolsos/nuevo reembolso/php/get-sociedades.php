<?php

    include_once('../../../assets/db/conexion.php');

    $conn = conect();
    $res = [];
    $sql = "SELECT * FROM [Reembolsos].[dbo].[Sociedades] WHERE Status = ?";
    $stmt = sqlsrv_prepare($conn, $sql, array('Activo'));
    $result = sqlsrv_execute($stmt);
    
    while( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC) ) {
        $res[] = $row;
    }

    echo json_encode( $res );

?>