<?php

    include_once('../../../assets/db/conexion.php');

    $conn = conect();
    $res = [];
    $data = array($_POST["tipo_reembolso"]);
    $sql = "SELECT TOP 1 * FROM [Reembolsos].[dbo].[Reembolsos_Header] WHERE TipoReembolso = ? ORDER BY Folio DESC";
    $stmt = sqlsrv_prepare($conn, $sql, $data);
    $result = sqlsrv_execute($stmt);
    
    while( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC) ) {
        $res[] = $row;
    }

    echo json_encode( $res );

?>