<?php

    include_once('../../../assets/db/conexion.php');

    $conn = conect();
    $res = [];    
    $data = array($_POST["folio"], $_POST["tipo-reembolso"], $_POST["nombre"], $_POST["periodo-inicio"], $_POST["periodo-final"], $_POST["status"], $_POST["area"], $_POST["asesor"]);    
    $sql = "INSERT INTO [Reembolsos].[dbo].[Reembolsos_Header] (Folio, TipoReembolso, Nombre, PeriodoInicio, PeriodoFin, Status, Area, Asesor) VALUES (?,?,?,?,?,?,?,?)";
    $stmt = sqlsrv_prepare($conn, $sql, $data);
    $result = sqlsrv_execute($stmt);
    
    if ( $result ) {
        $type = "success";
        $message = "Reembolso guardado con éxito";
        $mensaje_interno = null;
    } else {  
        $type = "danger";
        $message = "Error al guardar el reembolso";
        $mensaje_interno = sqlsrv_errors();      
    }
    
    $res = ['type' => $type, 'message' => $message, 'mnj_interno' => $mensaje_interno];
    echo json_encode( $res );

?>