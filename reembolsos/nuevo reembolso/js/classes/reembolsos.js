import {Formatter} from '/reembolsos/assets/js/plugins/currency-format.js';


export class Reembolsos {


    constructor() {
        this.stringMessage = "la partida";
    }

    postReembolso(form) {

        const FORM_DATA = new FormData(form)
        FORM_DATA.append('periodo-inicio', form.name = periodo.value.split(' - ')[0]);
        FORM_DATA.append('periodo-final', form.name = periodo.value.split(' - ')[1]);
        FORM_DATA.append('tipo_reembolso', document.querySelector('[name=tipo-reembolso]').value);
        FORM_DATA.append('folio', document.querySelector('[name=folio-reembolso]').value.split('-')[1]);
        fetch('php/insert-reembolso.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => { 
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })

    }

    authorizeReembolso() {

        const MSN = new Message('success', `Se autorizó el reembolso ${this.folio} correctamente`);
        MSN.showMessage();

        // const FORM_DATA = new FormData()
        // FORM_DATA.append('folio', this.folio)
        // fetch('php/authorize-reembolso.php', { method: 'POST', body: FORM_DATA })
        // .then(data => data.json())
        // .then(data => { 
        //     const MSN = new Message(data.type, data.message);
        //     MSN.showMessage();
        // })

    }

    rejectReembolso() {

        const MSN = new Message('success', `Se denegó el reembolso ${this.folio} correctamente`);
        MSN.showMessage();

        // const FORM_DATA = new FormData()
        // FORM_DATA.append('folio', this.folio)
        // fetch('php/reject-reembolso.php', { method: 'POST', body: FORM_DATA })
        // .then(data => data.json())
        // .then(data => { 
        //     const MSN = new Message(data.type, data.message);
        //     MSN.showMessage();
        // })

    }

    deletePartida() {

        let folio = document.querySelector('#id-to-delete').innerHTML;
        document.querySelectorAll('#tbl-xmls tbody tr').forEach(tr => {
            if (tr.children[1].textContent == folio) {
                tr.remove();
                $('#modal-delete-confirmation').modal('toggle');
            }
        })

    }

    getConsecutiveFolio() {

        const FORM_DATA = new FormData()
        FORM_DATA.append('tipo_reembolso', document.querySelector('[name=tipo-reembolso]').value)

        return fetch('php/get-consecutive.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {

            return data;
        })
    }

    copyXMLTmp(file) {
        
        const FORM_DATA = new FormData()
        FORM_DATA.append('file', file);

        //Create file tmp in dir tmpxml and return the tmp name
        return fetch('php/copy-tmp-xml.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => { 
           return data.tmp_name;
        })
        
    }

    getXMLData(temporal_name) {

        return fetch(`tmpxml/${temporal_name}`)
        .then(response => response.text())
        .then(data => {

            let parser = new DOMParser()
            return parser.parseFromString(data,"text/xml");

        });

    }

    async validateXMLDate(temporal_name) {
        
        let date = ``;
        const DATA_PARSER = await this.getXMLData(temporal_name);
        const COMPROBANTE = DATA_PARSER.getElementsByTagName("Comprobante")[0];
        const CFDI_COMPROBANTE = DATA_PARSER.getElementsByTagName("cfdi:Comprobante")[0];
        const CFDI_IMPUESTOS = DATA_PARSER.getElementsByTagName("cfdi:Impuestos")[0];

        COMPROBANTE ? date = COMPROBANTE.attributes.Fecha.value : date = CFDI_COMPROBANTE.attributes.Fecha.value;

        const XML_DATE = Date.parse(date); 
        const PERIODO = document.querySelector('#periodo').value.split(' - ');
        const FECHA_INICIO = Date.parse(PERIODO[0]);
        const FECHA_FINAL = Date.parse(PERIODO[1]);
        let response = false;

        XML_DATE >= FECHA_INICIO && XML_DATE <= FECHA_FINAL ? response = true : response;

        return response;

    }

    validateIDSociedad(temporal_name) {

        const DATA_PARSER = await this.getXMLData(temporal_name);
        const CFDI_RECEPTOR_NOMBRE = DATA_PARSER.getElementsByTagName("cfdi:Receptor Nombre")[0];
        let response = false
        console.log(CFDI_RECEPTOR_NOMBRE);
        const ID_SELECTED = document.querySelector('[name=id-sociedad]').value;

        CFDI_COMPROBANTE.attributes.Rfc.value == ID_SELECTED ? response = true : response;
        return response;
        
    }

    async addDataXMLToTable(temporal_name) {

        const FORMATTER = new Formatter();
        const DATA_PARSER = await this.getXMLData(temporal_name);
        const CONCEPTOS = await this.getOptionsConceptos(document.querySelector('[name=tipo-reembolso]').value);
        const CFDI_COMPROBANTE = DATA_PARSER.getElementsByTagName("cfdi:Comprobante")[0];
        const CFDI_COMPLEMENTO = DATA_PARSER.getElementsByTagName("tfd:TimbreFiscalDigital")[0];
        const CFDI_IMPUESTOS = DATA_PARSER.getElementsByTagName("cfdi:Impuestos")[1];
        let linea = ``;

        linea += `
            <tr>
                <td class="text-nowrap align-middle text-center"><button type="button" class="btn btn-sm btn-danger btn-delete" id="${CFDI_COMPROBANTE.attributes.Folio.value}" title="Quitar concepto" data-toggle="modal" data-target="#modal-delete-confirmation"><span class="material-icons align-middle">delete</span>Eliminar</button></td> 
                <td class="text-nowrap align-middle text-center">${CFDI_COMPROBANTE.attributes.Folio.value}</td> 
                <td class="text-nowrap align-middle text-center">${moment(CFDI_COMPROBANTE.attributes.Fecha.value).format('YYYY/MM/DD')}</td> 
                <td class="text-nowrap align-middle text-center">
                    <select class="form-control text-center" style="text-align-last: center; width: 200px">                                        
                        ${CONCEPTOS}
                    </select>
                </td> 
                <td class="text-nowrap align-middle text-center">$ ${FORMATTER.currencyFormat(CFDI_COMPROBANTE.attributes.SubTotal.value)}</td> 
                <td class="text-nowrap align-middle text-center">$ ${FORMATTER.currencyFormat(CFDI_IMPUESTOS.attributes.TotalImpuestosTrasladados.value)}</td> 
                <td class="text-nowrap align-middle text-center">$ ${FORMATTER.currencyFormat(CFDI_COMPROBANTE.attributes.Total.value)}</td> 
                <td class="text-nowrap align-middle text-center"></td> 
                <td class="text-nowrap align-middle text-center">${CFDI_COMPLEMENTO.attributes.UUID.value}</td>
                <td class="text-nowrap align-middle text-center"><textarea class="form-control" name="comentarios" rows="2" style="width: 230px; padding: 8px;"></textarea></td> 
                <td class="text-nowrap align-middle text-center"></td> 
                <td class="text-nowrap align-middle text-center">
                    <select class="form-control form-control-sm text-center" name="autorizar" style="text-align-last: center;">
                        <option value="si" selected>Si</option>
                        <option value="no">No</option>
                    </select>
                </td> 
            </tr>
        `
        return linea;

        

    }

    async setPartidas(temporal_name) {

        const LINEA = await this.addDataXMLToTable(temporal_name);
        const TBODY = document.querySelectorAll('#tbl-xmls tbody tr');
        TBODY.length ? $('#tbl-xmls tbody').append(LINEA) : document.querySelector('#tbl-xmls tbody').innerHTML = LINEA;
        
        // Itera todos los botones de eliminado para asignar el evento click.
        document.querySelectorAll('.btn-delete').forEach(button => {
            button.addEventListener('click', (event) => {                    
            document.querySelector("#delete-name").innerHTML = this.stringMessage;
            document.querySelector("#id-to-delete").innerHTML = event.target.id;
            this.rowToDelete = event.target.parentElement.parentElement;
            })
        });

    }

    deleteXMLTmp(temporal_name) {

        const FORM_DATA = new FormData();
        FORM_DATA.append('tmp_name', temporal_name);
        fetch('php/delete-xml-tmp.php', {method: 'POST', body: FORM_DATA })

    }

    async getOptionsConceptos(tipo_reembolso) {

        let conceptos = '<option value="">Selecciona Concepto</option>';
        switch (tipo_reembolso) {
            case 'RV':

                const DATA = await this.getConceptosViaticos();
                DATA.forEach(element => {
                    conceptos += `<option value="${element.Code}">${element.Name}</option>`;
                });
                
            break;
            case 'RC':
                // const DATA = await getConceptosCajaChica();
            break;
        }

        return conceptos; 

    }

    getConceptosViaticos() { 

        const FORM_DATA = new FormData()
        return fetch('php/get-conceptos-viaticos.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            return data;
        })

    }

    getConceptosCajaChica() {

        // const FORM_DATA = new FormData()
        // FORM_DATA.append('file', file)
        // return fetch('php/upload-xml.php', { method: 'POST', body: FORM_DATA })
        // .then(data => data.json())
        // .then(data => {
        //     return data;
        // })

    }

    async validateXMLAlreadyExist(temporal_name) {

        let response = false;
        const DATA_PARSER = await this.getXMLData(temporal_name);
        const CFDI_COMPROBANTE = DATA_PARSER.getElementsByTagName("cfdi:Comprobante")[0];
        const TBODY = document.querySelectorAll('#tbl-xmls tbody tr');

        TBODY.forEach(tr => {
            
            if (tr.children[1].textContent == CFDI_COMPROBANTE.attributes.Folio.value)
                response = true;

        });

        return response;

    }

}