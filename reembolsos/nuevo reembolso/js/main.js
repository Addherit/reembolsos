`use strict`;

import { Reembolsos } from "./classes/reembolsos.js";
import { Sociedades } from "./classes/sociedades.js";

document.addEventListener('DOMContentLoaded', () => { 
    getConsecutive();
    dateRangePickeer();
    setSociedades();
})

document.querySelector('#btn-save').addEventListener('click', () => { saveReembolso() });
document.querySelector('#btn-authorize').addEventListener('click', () => { authorizeReembolso() });
document.querySelector('#btn-reject').addEventListener('click', () => { rejectReembolso() });
document.querySelector('[name=tipo-reembolso]').addEventListener('change', ()=> { getConsecutive() });
document.querySelector('[name=tipo-reembolso]').addEventListener('click', ()=> { validateIfIsDisabled() });
document.querySelector('#btn-upload-xml').addEventListener('change', () => { addXML() });
document.querySelector('#btn-delete').addEventListener('click', () => { deletePartidaXML() })

function disabledTipoReembolso() {

    const TIPO_REEMBOLSO = document.querySelector('[name=tipo-reembolso]');
    if (TIPO_REEMBOLSO.disabled != true) { TIPO_REEMBOLSO.disabled = true }

}

function validateIfIsDisabled() {

    const TIPO_REEMBOLSO = document.querySelector('[name=tipo-reembolso]');
    if (TIPO_REEMBOLSO.disabled == true) {        
        const MSN = new Message('warning', `No se puede cambiar el tipo de reembolso una vez ingresado un xml`);
        MSN.showMessage();
    }

}

function saveReembolso() {
    
    const REEMBOLSO = new Reembolsos();
    REEMBOLSO.postReembolso(document.querySelector('#form-header-reembolso'));

}

function authorizeReembolso() {

    const FOLIO = document.querySelector('[name=folio-reembolso]').value;
    const REEMBOLSO = new Reembolsos();
    REEMBOLSO.authorizeReembolso();
    
}

function rejectReembolso() {

    const FOLIO = document.querySelector('[name=folio-reembolso]').value;
    const REEMBOLSO = new Reembolsos();
    REEMBOLSO.rejectReembolso();
    
}

async function getConsecutive() {
    
    const REEMBOLSO = new Reembolsos()
    const FOLIO = await REEMBOLSO.getConsecutiveFolio();
    if (FOLIO.length) {
        document.querySelector('[name=folio-reembolso]').value = `${FOLIO[0].TipoReembolso}-${parseInt(FOLIO[0].Folio) + 1}`
    } else {
        document.querySelector('[name=folio-reembolso]').value = `${document.querySelector('[name=tipo-reembolso]').value}-1`
    }    

}


function dateRangePickeer() {
    
    // Activate datePicker Plugin with out value in input
    $('.date-range').daterangepicker({
        // showDropdowns: true, //mostrar dropdowns of moths and years
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });
  
    $('.date-range').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
    });
  
    $('.date-range').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    //Esto se hace solo para cambiar el label del botón pues desde el plugin no se pudo. 
    document.querySelector('.cancelBtn').innerHTML = 'Limpiar';

}

async function addXML() {
    
        // validate range date exist
        if (document.querySelector('#periodo').value && document.querySelector('[name=id-sociedad]').value) {
        
            for (let key = 0; key < document.querySelector('#btn-upload-xml').files.length; key++) {

                const FILE = document.querySelector('#btn-upload-xml').files[key];
                const REEMBOLSO = new Reembolsos();
                const TMP_NAME = await REEMBOLSO.copyXMLTmp(FILE);                

                // validate created file tmp correctly
                if (TMP_NAME) {
                    
                    // validate if already exist this xml
                    if ( ! await REEMBOLSO.validateXMLAlreadyExist(TMP_NAME)) {

                        // validate if xml date is between range date ---- return true/false
                        if (await REEMBOLSO.validateXMLDate(TMP_NAME)) {
                            
                            // validate id de sociedad with id de sociedad xml ---- return true/false
                            if (await REEMBOLSO.validateIDSociedad(TMP_NAME)) {
                                
                                insertPartidaXML(TMP_NAME);
                                setTimeout(() => {
                                    deleteTmp(TMP_NAME);
                                }, 2000);
                                

                            } else {
                                const MSN = new Message('warning', `El ID de sociedad seleccionado no coincide con el del XML`);
                                MSN.showMessage();
                            }

                        } else {
                            const MSN = new Message('warning', `La fecha del xml no está dentro del rango seleccionado`);
                            MSN.showMessage();
                        }
                    } else {

                        const MSN = new Message('warning', `El xml que intenta ingresar, ya está en la lista`);
                        MSN.showMessage();
                    }
                    
                } else {
                    const MSN = new Message('danger', `No se pudo crear el archivo temporal`);
                    MSN.showMessage();
                }
                
            }

        } else {
            const MSN = new Message('warning', `Primero selecciona un periodo y una sociedad, luego agregas un XML`);
            MSN.showMessage();
            document.querySelector('#btn-upload-xml').value = '';
        }
        

}

async function insertPartidaXML(tmp_name) {

    const REEMBOLSO = new Reembolsos();
    REEMBOLSO.setPartidas(tmp_name);
    if (document.querySelectorAll('#tbl-xmls tbody tr').length) {
        disabledTipoReembolso();
    }
    

}

function deletePartidaXML() {

    const REEMBOLSO = new Reembolsos();
    REEMBOLSO.deletePartida()

}

async function deleteTmp(tmp_name) {

    const REEMBOLSO = new Reembolsos();
    await REEMBOLSO.deleteXMLTmp(tmp_name);

}

async function setSociedades() {

    const SOCIEDADES = new Sociedades();
    const DATA = await SOCIEDADES.getSociedades();
    let options = '<option value="" selected disabled>Selecciona una sociedad</option>';

    DATA.forEach(sociedad => {
        options += `<option value="${sociedad.RFC}">${sociedad.Name}</option>`
    });
    document.querySelector('[name=id-sociedad]').innerHTML = options;

}


  