<!DOCTYPE html>
<html lang="en">
    <?php include "../../includes/header.html" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg1 toggled"> 
            <?php include "../../assets/modales/modal-delete-confirmation.html" ?>       
            <?php include "./modal-add-xml.html"?>
            <?php include "./modal-add-xml-masive.html"?>
            <?php include "../../sidebar/index.html"?> 
            
            <!-- page-content  -->
            <main class="page-content pt-2">
              
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="form-group col-12">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#" title="Ocultar/Mostrar menú">
                            <i class="fas fa-bars fa-lg"></i>
                          </a>
                            <span class="h1 align-middle" id="title-page">NUEVO REEMBOLSO</span>
                        </div>                        
                    </div>
                    <hr> 
                    <div class="row mb-2">
                        <div class="col-12">
                            <img src="/reembolsos/assets/img/header-new-reembolso.png" alt="header" style="width: inherit;">
                        </div>
                    </div>
                    <form action="" id="form-header-reembolso">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <label for="id-sociedad" class="col-sm-2 col-form-label">ID de sociedad</label>
                                    <div class="col-sm-10">
                                        <select class="form-control form-control-sm text-center" name="id-sociedad"></select>
                                    </div>
                                </div>
                            </div>                       
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-12">
                                    <select class="form-control text-center" style="text-align-last: center; background-color: #004c6c; color: white" name="tipo-reembolso">                                        
                                        <option value="RC">REEMBOLSO DE CAJA CHICA</option>
                                        <option value="RV">REEMBOLSO VIÁTICO</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="row">
                                    <label for="folio-reembolso" class="col-md-4 col-form-label">Folio</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control form-control-sm" name="folio-reembolso" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="nombre" class="col-md-4 col-form-label">Nombre</label>
                                    <div class="col-md-8">
                                        <input class="form-control form-control-sm" list="list-nombres" name="nombre" placeholder="Escribe un nombre">
                                        <datalist id="list-nombres">
                                            <option value="San Francisco">
                                            <option value="New York">
                                            <option value="Seattle">
                                            <option value="Los Angeles">
                                            <option value="Chicago">
                                        </datalist>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="periodo" class="col-md-4 col-form-label">Periodo</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control form-control-sm date-range" id="periodo">
                                    </div>
                                </div>      
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <label for="status" class="col-md-4 col-form-label">Status</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control form-control-sm" name="status" value="Solicitud" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="area" class="col-md-4 col-form-label">Área</label>
                                    <div class="col-md-8">
                                        <input class="form-control form-control-sm" list="list-areas" name="area" placeholder="Escribe una área">
                                        <datalist id="list-areas">
                                            <option value="Contabilidad">
                                            <option value="Tesorería">
                                            <option value="Diseño">
                                            <option value="Cobranza">
                                            <option value="Operaciones">
                                            <option value="Ventas">
                                            <option value="Almacén">
                                            <option value="Sistemas">
                                        </datalist>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="asesor" class="col-md-4 col-form-label">No. Asesor</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control form-control-sm" name="asesor" placeholder="No Asesor">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <label for="ruta" class="col-md-2 col-form-label">Ruta</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control form-control-sm" name="ruta" placeholder="Escribe una Ruta">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <br>
                    <div class="row mb-2">
                        <div class="col-12 text-right">
                            <input type="file" name="btn-upload-xml" id="btn-upload-xml" class="inputfile" multiple>
                            <label for="btn-upload-xml"> <i class="fas fa-upload"></i> Agregar xml</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12" style="width: 100%; overflow: auto">
                            <table class="table table-sm table-hover" id="tbl-xmls">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Folio</th>
                                        <th>Fecha</th>
                                        <th>Concepto</th>
                                        <th>Importe</th>
                                        <th>Impuestos</th>
                                        <th>Total</th>
                                        <th>Otros</th>
                                        <th>UUID</th>
                                        <th>Observaciones</th>
                                        <th>Validación</th>
                                        <th>Autorizar</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>

                    <form action="" id="form-footer-reembolso">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <label for="inputtext" class="col-md-4 col-form-label">Recurso Proporcionado</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control form-control-sm" name="recursos-proporcionados" placeholder="$ 0.00">
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="inputtext" class="col-md-4 col-form-label">Diferencia</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control form-control-sm" name="diferencia" readonly>
                                    </div>
                                </div>      
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Comentarios del reembolso</label>
                                    <textarea class="form-control" name="comentarios" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-12 text-right">
                            <button class="btn btn-sm btn-success mb-1" id="btn-attached"><i class="fas fa-plus"></i> Agregar anexos</button>
                            <button class="btn btn-sm btn-success mb-1" id="btn-save"><i class="fas fa-save"></i> Guardar</button>
                            <button class="btn btn-sm btn-success mb-1" id="btn-authorize"><i class="far fa-thumbs-up"></i> Autorizar</button>
                            <button class="btn btn-sm btn-danger mb-1" id="btn-reject"><i class="fas fa-ban"></i> Denegar</button>
                        </div>
                    </div>
                    
                  
                </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>

    <?php include "../../includes/footer.html" ?>    
    <script src="js/main.js" type="module"></script>

      
     
</html>