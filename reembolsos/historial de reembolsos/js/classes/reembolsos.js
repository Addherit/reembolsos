
import {DataTable} from '/reembolsos/assets/js/classes/dataTable.js';
import {Formatter} from '/reembolsos/assets/js/plugins/currency-format.js';

export class Reembolsos {

  constructor() {      
      this.stringMessage = "el reembolso";
      this.rowToDelete = ``;
  }

  getReembolsos() {

    const FORMATTER = new Formatter();
    return fetch('php/select-reembolsos.php')
    .then(data => data.json())
    .then(data => {
      let linea = ``;
      data.forEach(reembolso => {
        linea += `
          <tr>
            <td class="text-nowrap align-middle text-center">
              <button type="button" class="btn btn-sm btn-danger btn-delete" title="Eliminar Reembolso" id="${reembolso.TipoReembolso}-${reembolso.Folio}" data-toggle="modal" data-target="#modal-delete-confirmation"><span class="material-icons align-middle">delete</span>Eliminar</button>
              <button type="button" class="btn btn-sm btn-warning btn-edit" title="Editar Reembolso" id="${reembolso.TipoReembolso}-${reembolso.Folio}"><span class="material-icons align-middle">edit</span>Editar</button>
            </td> 
            <td>${reembolso.TipoReembolso}-${reembolso.Folio}</td>            
            <td>${reembolso.Nombre}</td>
            <td>${reembolso.PeriodoInicio.date.split(' ')[0]} // ${reembolso.PeriodoFin.date.split(' ')[0]}</td>
            <td>$ ${FORMATTER.currencyFormat(reembolso.MontoAautorizado)}</td>
            <td>$ ${FORMATTER.currencyFormat(reembolso.MontoPorporcionado)}</td>
            <td>${reembolso.Status}</td>
          </tr>
        `
      });
    return linea;
    })

  }

  async setReembolsos() {

    const rows = await this.getReembolsos();
    document.querySelector(`#tbl-reembolsos tbody`).innerHTML = rows;
    const dataTable = new DataTable('tbl-reembolsos');
    dataTable.initDataTable();

    // Itera todos los botones de editar para asignar el evento click.
    document.querySelectorAll('.btn-edit').forEach(button => {
      button.addEventListener('click', (event) => {
        let array_folio = event.target.id.split('-');
        window.location.href = `/reembolsos/reembolsos/editar reembolso/index.php?folio=${array_folio[1]}&tipo_reembolso=${array_folio[0]}`;
      })
    });

     // Itera todos los botones de eliminado para asignar el evento click.
     document.querySelectorAll('.btn-delete').forEach(button => {
      button.addEventListener('click', (event) => {
        document.querySelector("#delete-name").innerHTML = this.stringMessage
        document.querySelector("#id-to-delete").innerHTML = event.target.id;
        this.rowToDelete = event.target.parentElement.parentElement;
      })
    });

  }
    
  deleteReembolso() {

    let array_folio = document.querySelector('#id-to-delete').innerHTML.split('-');
    this.rowToDelete.remove();
    
    setTimeout(() => {
      const dataTable = new DataTable('tbl-reembolsos');
      dataTable.initDataTable();
    }, 2000);
    $('#modal-delete-confirmation').modal('toggle');

    // aqui va la llamada al php
    const FORM_DATA = new FormData()
    FORM_DATA.append('id_reembolso', array_folio[1])
    FORM_DATA.append('tipo_reembolso',array_folio[0])
    fetch('php/delete-reembolso.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
      const MSN = new Message(data.type, data.message);
      MSN.showMessage();      
    })

  }

}