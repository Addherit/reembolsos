`use strict`;

import {Reembolsos} from './classes/reembolsos.js';

const REEMBOLSOS = new Reembolsos()

document.addEventListener('DOMContentLoaded', () => { REEMBOLSOS.setReembolsos() })
document.querySelector('#btn-delete').addEventListener('click', () => { REEMBOLSOS.deleteReembolso() })
