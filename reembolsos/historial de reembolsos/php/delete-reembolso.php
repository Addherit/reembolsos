<?php

    include_once('../../../assets/db/conexion.php');

    $conn = conect();
    $res = [];    
    $formData = array('Eliminado', $_POST['id_reembolso'], $_POST['tipo_reembolso']);    
    $sql = "UPDATE [Reembolsos].[dbo].[Reembolsos_Header] SET Status = ? WHERE Folio = ? AND TipoReembolso = ?";
    $stmt = sqlsrv_prepare($conn, $sql, $formData);
    $result = sqlsrv_execute($stmt);
    
    if ( $result ) {
        $type = "success";
        $message = "Eliminado correctamente";
        $mensaje_interno = null;
    } else {  
        $type = "danger";
        $message = "Error al eliminar el reembolso";
        $mensaje_interno = sqlsrv_errors();      
    }
    
    $res = ['type' => $type, 'message' => $message, 'mnj_interno' => $mensaje_interno];
    echo json_encode( $res );

?>