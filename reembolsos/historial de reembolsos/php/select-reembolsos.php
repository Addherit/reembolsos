<?php

    include_once('../../../assets/db/conexion.php');

    $conn = conect();
    $res = [];
    $query = "SELECT * FROM [Reembolsos].[dbo].[Reembolsos_Header] WHERE Status != 'Eliminado'";
    $stmt = sqlsrv_query($conn, $query);
    
    while( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC) ) {
        $res[] = $row;
    }

    echo json_encode( $res );

?>