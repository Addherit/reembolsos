`use strict`;

// import { Reembolsos } from "./classes/reembolsos.js";

// document.querySelector('#btn-save').addEventListener('click', () => { saveReembolso() });
// document.querySelector('#btn-authorize').addEventListener('click', () => { authorizeReembolso() });
// document.querySelector('#btn-reject').addEventListener('click', () => { rejectReembolso() });

// function saveReembolso() {

//     const FOLIO = document.querySelector('[name=folio-reembolso]').value;
//     const REEMBOLSO = new Reembolsos(FOLIO);
//     REEMBOLSO.postReembolso(document.querySelector('#form-header-reembolso'));
    
// }

function authorizeReembolso() {

    const FOLIO = document.querySelector('[name=folio-reembolso]').value;
    const REEMBOLSO = new Reembolsos(FOLIO);
    REEMBOLSO.authorizeReembolso();
    
}

function rejectReembolso() {

    const FOLIO = document.querySelector('[name=folio-reembolso]').value;
    const REEMBOLSO = new Reembolsos(FOLIO);
    REEMBOLSO.rejectReembolso();
    
}

document.addEventListener('DOMContentLoaded', () => {
    
    document.querySelector('#folio-reembolso').innerHTML = getFolioFromURL();
    
})

function getFolioFromURL() {
    
    const PARAMS_URL = new URLSearchParams(window.location.search);
    return `${PARAMS_URL.get('tipo_reembolso')}-${PARAMS_URL.get('folio')}`;

}
