export class Reembolsos {


    constructor(folio) {
        this.folio = folio;
    }

    postReembolso(form) {

        const MSN = new Message('success', `Se guardó el reembolso ${this.folio} correctamente`);
        MSN.showMessage();

        // const FORM_DATA = new FormData(form)
        // fetch('php/insert-reembolso.php', { method: 'POST', body: FORM_DATA })
        // .then(data => data.json())
        // .then(data => { 
        //     const MSN = new Message(data.type, data.message);
        //     MSN.showMessage();
        // })

    }

    authorizeReembolso() {

        const MSN = new Message('success', `Se autorizó el reembolso ${this.folio} correctamente`);
        MSN.showMessage();

        // const FORM_DATA = new FormData()
        // FORM_DATA.append('folio', this.folio)
        // fetch('php/authorize-reembolso.php', { method: 'POST', body: FORM_DATA })
        // .then(data => data.json())
        // .then(data => { 
        //     const MSN = new Message(data.type, data.message);
        //     MSN.showMessage();
        // })

    }

    rejectReembolso() {

        const MSN = new Message('success', `Se denegó el reembolso ${this.folio} correctamente`);
        MSN.showMessage();

        // const FORM_DATA = new FormData()
        // FORM_DATA.append('folio', this.folio)
        // fetch('php/reject-reembolso.php', { method: 'POST', body: FORM_DATA })
        // .then(data => data.json())
        // .then(data => { 
        //     const MSN = new Message(data.type, data.message);
        //     MSN.showMessage();
        // })

    }
}