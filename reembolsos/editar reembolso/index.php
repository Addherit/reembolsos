<!DOCTYPE html>
<html lang="en">
    <?php include "../../includes/header.html" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg1 toggled"> 
            <?php include "../../assets/modales/modal-delete-confirmation.html" ?>       
            <?php include "./modal-add-item.html"?>
            <?php include "../../sidebar/index.html"?> 
            
            <!-- page-content  -->
            <main class="page-content pt-2">
              
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="form-group col-12">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#" title="Ocultar/Mostrar menú">
                            <i class="fas fa-bars fa-lg"></i>
                          </a>
                            <span class="h1 align-middle" id="title-page">EDITAR REEMBOLSO <span id="folio-reembolso"></span></span>
                        </div>                        
                    </div>
                    <hr> 
                    <div class="row mb-2">
                        <div class="col-12">
                            <img src="/reembolsos/assets/img/header-new-reembolso.png" alt="header" style="width: inherit;">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group row">
                                <label for="id-sociedad" class="col-sm-2 col-form-label">ID de sociedad</label>
                                <div class="col-sm-10">
                                    <select class="form-control form-control-sm text-center" name="id-sociedad">
                                        <option value="" selected disabled>Selecciona una sociedad</option>
                                    </select>
                                </div>
                            </div>
                        </div>                       
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group row">
                                <div class="col-12">
                                <select class="form-control text-center" style="text-align-last: center; background-color: #004c6c; color: white">
                                    <option value="" selected disabled>SELECCIONA UN TIPO DE REEMBOLSO</option>
                                    <option value="rcch">REEMBOLSO DE CAJA CHICA</option>
                                    <option value="rv">REEMBOLSO VIÁTICO</option>
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="" id="form-header-reembolso">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="row">
                                    <label for="folio-reembolso" class="col-md-4 col-form-label">Folio</label>
                                    <div class="col-md-8">
                                        <input type="password" class="form-control form-control-sm" name="folio-reembolso" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="nombre" class="col-md-4 col-form-label">Nombre</label>
                                    <div class="col-md-8">
                                        <input class="form-control form-control-sm" list="list-nombres" name="nombre" placeholder="Escribe un nombre">
                                        <datalist id="list-nombres">
                                            <option value="San Francisco">
                                            <option value="New York">
                                            <option value="Seattle">
                                            <option value="Los Angeles">
                                            <option value="Chicago">
                                        </datalist>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="periodo" class="col-md-4 col-form-label">Periodo</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control form-control-sm" id="periodo" placeholder="Password">
                                    </div>
                                </div>      
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <label for="status" class="col-md-4 col-form-label">Status</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control form-control-sm" name="status" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="area" class="col-md-4 col-form-label">Área</label>
                                    <div class="col-md-8">
                                        <input class="form-control form-control-sm" list="list-areas" name="area" placeholder="Escribe una área">
                                        <datalist id="list-areas">
                                            <option value="Contabilidad">
                                            <option value="Tesorería">
                                            <option value="Diseño">
                                            <option value="Cobranza">
                                            <option value="Operaciones">
                                            <option value="Ventas">
                                            <option value="Almacén">
                                            <option value="Sistemas">
                                        </datalist>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="asesor" class="col-md-4 col-form-label">No. Asesor</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control form-control-sm" name="asesor" placeholder="No Asesor">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row mb-2">
                        <div class="col-12 text-right">
                            <button class="btn btn-sm btn-info">Agregar xml masivo</button>
                            <button class="btn btn-sm btn-info">Agregar xml</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-sm table-hover">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Fecha</th>
                                        <th>Concepto</th>
                                        <th>Importe</th>
                                        <th>Impuestos</th>
                                        <th>Total</th>
                                        <th>Otros</th>
                                        <th>UllD</th>
                                        <th>Observaciones</th>
                                        <th>Validación</th>
                                        <th>Autorizar</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <form action="" id="form-footer-reembolso">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <label for="inputPassword" class="col-md-4 col-form-label">Recurso Proporcionado</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control form-control-sm" name="recursos-proporcionados" placeholder="$ 0.00">
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="inputPassword" class="col-md-4 col-form-label">Diferencia</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control form-control-sm" name="diferencia" readonly>
                                    </div>
                                </div>      
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Comentarios del reembolso</label>
                                    <textarea class="form-control" name="comentarios" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-12 text-right">
                            <button class="btn btn-sm btn-success" id="btn-attached">Agregar anexos</button>
                            <button class="btn btn-sm btn-success" id="btn-save">Guardar</button>
                            <button class="btn btn-sm btn-success" id="btn-authorize">Autorizar</button>
                            <button class="btn btn-sm btn-danger" id="btn-reject">Denegar</button>
                        </div>
                    </div>
                    
                  
                </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>

    <?php include "../../includes/footer.html" ?>    
    <script src="js/main.js" type="module"></script>

      
     
</html>