class NumNotify {

    initAllNotyfys() {
        
        this.getNotifyHistorialReembolsos();

    }

    getNotifyHistorialReembolsos() {

        return fetch('/reembolsos/reembolsos/historial de reembolsos/php/select-reembolsos.php')
        .then(data => data.json())
        .then(data => {        
            document.querySelector('#notify-historial-rembolsos').innerHTML = data.length;
        })

    }
    
}


document.addEventListener('DOMContentLoaded', () => { 

    const NOTIFY = new NumNotify();
    NOTIFY.initAllNotyfys() 

})


