export class Formatter {

    //numberFormat replace coma to none
    numberFormat(data) {
        const NUMBER = data.replace(',', '');
        return NUMBER;

    }

    //currencyFormat method use for convert number with coma and point (decimal format) see documentation in website
    currencyFormat(data) {

        const formatter = new Intl.NumberFormat('en-US', {
            style: 'decimal',
            minimumFractionDigits: 2,
        });
        const CURRENCY = formatter.format(data);
        return CURRENCY;

    }

}
